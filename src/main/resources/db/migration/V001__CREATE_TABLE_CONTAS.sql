CREATE SEQUENCE contas_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;


CREATE TABLE contas
(
  id integer NOT NULL DEFAULT nextval('contas_id_seq'::regclass),
  nome character varying(200) NOT NULL,
  dataVencimento timestamp without time zone NOT NULL,
  dataPagamento timestamp without time zone NOT NULL,
  valorOriginal numeric(5,2) NOT NULL,
  valorCorrigido numeric(5,2) NOT NULL,
  diasAtrasado integer NOT NULL,
  regraAtraso integer NOT NULL,
  CONSTRAINT pk_categories PRIMARY KEY (id)
);