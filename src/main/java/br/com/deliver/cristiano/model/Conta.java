package br.com.deliver.cristiano.model;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.deliver.cristiano.enums.RegraAtraso;

@Entity
@Table(name = "contas")
@SequenceGenerator(name = "contas_id_seq", sequenceName = "contas_id_seq", allocationSize = 1)
public class Conta {
	
	@Id
	@GeneratedValue(generator = "contas_id_seq", strategy = GenerationType.SEQUENCE)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "datavencimento")
	private Calendar dataVencimento;
	
	@Column(name = "datapagamento")
	private Calendar dataPagamento;
	
	@Column(name = "valororiginal")
	private Double valorOriginal;
	
	@Column(name = "valorcorrigido")
	private Double valorCorrigido;
	
	@Column(name = "diasatrasado")
	private Integer diasAtrasado;
	
	@Column(name = "regraatraso")
	private RegraAtraso regraAtraso;
	
	public Conta() {
	}
	
	public Conta(String nome, Double valorOriginal, Calendar dataVencimento, Calendar dataPagamento, Double valorCorrigido, Integer diasAtrasado, RegraAtraso regraAtraso) {
		this.nome = nome;
		this.valorOriginal = valorOriginal;
		this.dataVencimento = dataVencimento;
		this.dataPagamento = dataPagamento;
		this.valorCorrigido = valorCorrigido;
		this.diasAtrasado = diasAtrasado;
		this.regraAtraso = regraAtraso;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Calendar getDataVencimento() {
		return dataVencimento;
	}
	public void setDataVencimento(Calendar dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	
	public Calendar getDataPagamento() {
		return dataPagamento;
	}
	
	public void setDataPagamento(Calendar dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	
	public Double getValorOriginal() {
		return valorOriginal;
	}
	
	public void setValorOriginal(Double valorOriginal) {
		this.valorOriginal = valorOriginal;
	}
	
	public Double getValorCorrigido() {
		return valorCorrigido;
	}
	
	public void setValorCorrigido(Double valorCorrigido) {
		this.valorCorrigido = valorCorrigido;
	}
	
	public Integer getDiasAtrasado() {
		return diasAtrasado;
	}
	
	public void setDiasAtrasado(Integer diasAtrasado) {
		this.diasAtrasado = diasAtrasado;
	}
	
	public RegraAtraso getRegraAtraso() {
		return regraAtraso;
	}
	
	public void setRegraAtraso(RegraAtraso regraAtraso) {
		this.regraAtraso = regraAtraso;
	}
}
