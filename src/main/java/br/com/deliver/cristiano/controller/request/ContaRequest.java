package br.com.deliver.cristiano.controller.request;

import java.time.temporal.ChronoUnit;
import java.util.Calendar;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.deliver.cristiano.enums.RegraAtraso;
import br.com.deliver.cristiano.model.Conta;

public class ContaRequest {
	
	@NotNull @NotEmpty
	private String nome;
	@NotNull
	private Double valorOriginal;
	@NotNull
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Calendar dataVencimento;
	@NotNull
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Calendar dataPagamento;
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Double getValorOriginal() {
		return valorOriginal;
	}
	
	public void setValorOriginal(Double valorOriginal) {
		this.valorOriginal = valorOriginal;
	}
	
	public Calendar getDataVencimento() {
		return dataVencimento;
	}
	
	public void setDataVencimento(Calendar dataVencimento) {
		this.dataVencimento = dataVencimento;
	}
	
	public Calendar getDataPagamento() {
		return dataPagamento;
	}
	
	public void setDataPagamento(Calendar dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	
	public Conta requestToEntity() {
		Integer diasAtraso = getDiasAtraso(dataVencimento, dataPagamento) <= 0 ? 0 : getDiasAtraso(dataVencimento, dataPagamento);
		RegraAtraso regraAtraso = defineRegraAtraso(diasAtraso);
		Double valorCorrigido = getValorCorrigido(diasAtraso, regraAtraso);

		return new Conta(nome, valorOriginal, dataVencimento, dataPagamento, valorCorrigido, diasAtraso, regraAtraso);
	}
	
	private Integer getDiasAtraso(Calendar vencimento, Calendar pagamento) {
		return Long.valueOf(ChronoUnit.DAYS.between(dataVencimento.toInstant(), dataPagamento.toInstant())).intValue();
	}
	
	private RegraAtraso defineRegraAtraso(Integer diasAtraso) {
		if(diasAtraso <= 0) {
			return RegraAtraso.NAO_VENCIDA;
		} else {
			if(diasAtraso < 3) {
				return RegraAtraso.ATE_3_DIAS;
			} else if(diasAtraso >= 3 && diasAtraso < 5) {
				return RegraAtraso.MAIS_DE_3_DIAS;
			} else {
				return RegraAtraso.MAIS_DE_5_DIAS;
			}		
		}
	}
	
	private Double getValorCorrigido(Integer diasAtraso, RegraAtraso regraAtraso) {
		return valorOriginal + (valorOriginal * (regraAtraso.getMulta() / 100)) + (valorOriginal * ((regraAtraso.getJurosPorDia() / 100) * diasAtraso));
	}

}
