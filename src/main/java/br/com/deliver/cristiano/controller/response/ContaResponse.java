package br.com.deliver.cristiano.controller.response;

import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;

import br.com.deliver.cristiano.model.Conta;

public class ContaResponse {
	
	private Long id;
	private String nome;
	@JsonFormat(pattern = "dd/MM/yyyy")
	private Calendar dataPagamento;
	private Double valorOriginal;
	private Double valorCorrigido;
	private Integer diasAtrasado;
	
	public ContaResponse(Conta conta) {
		this.id = conta.getId();
		this.nome = conta.getNome();
		this.dataPagamento = conta.getDataPagamento();
		this.valorOriginal = conta.getValorOriginal();
		this.valorCorrigido = conta.getValorCorrigido();
		this.diasAtrasado = conta.getDiasAtrasado();
	}

	public Long getId() {
		return id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public Calendar getDataPagamento() {
		return dataPagamento;
	}
	
	public Double getValorOriginal() {
		return valorOriginal;
	}
	
	public Double getValorCorrigido() {
		return valorCorrigido;
	}
	
	public Integer getDiasAtrasado() {
		return diasAtrasado;
	}
	
	public static List<ContaResponse> entityToResponseList(List<Conta> contas){
		return contas.stream().map(ContaResponse::new).collect(Collectors.toList());
	}

}
