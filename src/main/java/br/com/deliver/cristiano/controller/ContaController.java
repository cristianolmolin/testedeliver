package br.com.deliver.cristiano.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.deliver.cristiano.controller.request.ContaRequest;
import br.com.deliver.cristiano.controller.response.ContaResponse;
import br.com.deliver.cristiano.model.Conta;
import br.com.deliver.cristiano.repository.ContaRepository;

@RestController
@RequestMapping("/contas")
public class ContaController {
	
	@Autowired
	private ContaRepository repository;
	
	/**
	 * EXEMPLO DE POST
	 * 
	 * {
			"nome": "teste", 
			"valorOriginal": 100.0, 
			"dataVencimento": "10/12/2019", 
			"dataPagamento": "16/12/2019"
		}
	 * 
	 * @return
	 */
	
	@GetMapping
	public List<ContaResponse> getContas(){
		return ContaResponse.entityToResponseList(repository.findAll());
	}
	
	@PostMapping
	public ResponseEntity<ContaResponse> saveConta(@RequestBody @Valid ContaRequest contaRequest, UriComponentsBuilder uriBuilder) {
		Conta conta = repository.save(contaRequest.requestToEntity());
		
		URI uri = uriBuilder.path("/contas/{id}").buildAndExpand(conta.getId()).toUri();
		return ResponseEntity.created(uri).body(new ContaResponse(conta));
	}

}
