package br.com.deliver.cristiano.enums;

public enum RegraAtraso {
	
	NAO_VENCIDA(1, 0.0, 0.0),
	ATE_3_DIAS(2, 2.0, 0.1),
	MAIS_DE_3_DIAS(3, 3.0, 0.2),
	MAIS_DE_5_DIAS(4, 5.0, 0.3);
	
	private Integer cod;
	private Double multa;
	private Double jurosPorDia;
	
	private RegraAtraso(Integer cod, Double multa, Double jurosPorDia) {
		this.cod = cod;
		this.multa = multa;
		this.jurosPorDia = jurosPorDia;
	}
	
	public Integer getCod() {
		return cod;
	}
	
	public Double getMulta() {
		return multa;
	}
	
	public Double getJurosPorDia() {
		return jurosPorDia;
	}

}
