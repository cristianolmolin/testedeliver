package br.com.deliver.cristiano.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.deliver.cristiano.model.Conta;

public interface ContaRepository extends JpaRepository<Conta, Long>{

}
